async function messages(parent, args, context) {
  const where = args.filter
    ? {
        text: args.filter,
      }
    : {};

  const messageList = await context.prisma.messages({
    where,
    skip: args.skip,
    first: args.first,
    orderBy: args.orderBy,
  });

  const count = await context.prisma
    .messagesConnection({
      where,
    })
    .aggregate()
    .count();

  return {
    currentPage: args.skip,
    count,
    messageList,
  };
}

module.exports = {
  messages,
};
