function postMessage(_, args, context) {
  return context.prisma.createMessage({
    text: args.text,
    likes: 0,
    dislikes: 0,
  });
}

async function postReview(_, args, context) {
  checkIfMessageExists(context, args.messageId);

  return context.prisma.createReview({
    text: args.text,
    message: { connect: { id: args.messageId } },
  });
}

async function postGiveLike(_, args, context) {
  await checkIfMessageExists(context, args.messageId);

  const message = await context.prisma.message({
    id: args.messageId,
  });

  return context.prisma.updateMessage({
    data: {
      likes: ++message.likes,
    },
    where: {
      id: args.messageId,
    },
  });
}

async function postGiveDislike(_, args, context) {
  await checkIfMessageExists(context, args.messageId);

  const message = await context.prisma.message({
    id: args.messageId,
  });

  return context.prisma.updateMessage({
    data: {
      dislikes: ++message.dislikes,
    },
    where: {
      id: args.messageId,
    },
  });
}

async function checkIfMessageExists(context, messageId) {
  const messageExists = await context.prisma.$exists.message({
    id: messageId,
  });

  if (!messageExists) {
    throw new Error(`Message with ID ${messageId} doesn't exists`);
  }

  return true;
}

module.exports = {
  postMessage,
  postReview,
  postGiveLike,
  postGiveDislike,
};
