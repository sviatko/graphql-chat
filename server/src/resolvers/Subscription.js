function newMessageSubscribe(parent, args, context, info) {
  return context.prisma.$subscribe
    .message({
      mutation_in: ["CREATED"],
    })
    .node();
}

const newMessage = {
  subscribe: newMessageSubscribe,
  resolve: (payload) => payload,
};

function newReviewSubscribe(parent, args, context, info) {
  return context.prisma.$subscribe
    .review({
      mutation_in: ["CREATED"],
    })
    .node();
}

const newReview = {
  subscribe: newReviewSubscribe,
  resolve: (payload) => payload,
};

function onUpdateSubscribe(parent, args, context) {
  return context.prisma.$subscribe
    .message({
      mutation_in: ["UPDATED"],
    })
    .node();
}

const update = {
  subscribe: onUpdateSubscribe,
  resolve: (payload) => payload,
};

module.exports = { newMessage, update, newReview };
