const { GraphQLServer } = require("graphql-yoga");
const Query = require("./resolvers/Query");
const Mutation = require("./resolvers/Mutation");
const Subscription = require("./resolvers/Subscription");
const Message = require("./resolvers/Message");
const Review = require("./resolvers/Review");
const prisma = require("./generated/prisma-client");

const resolvers = {
  Query,
  Mutation,
  Subscription,
  Message,
  Review,
};

const server = new GraphQLServer({
  typeDefs: "./src/schema.graphql",
  resolvers,
  context: prisma,
});

server.start(() => console.log("http://localhost:4000"));
