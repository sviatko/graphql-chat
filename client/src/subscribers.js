import {
  NEW_MESSAGE_SUBSCRIPTION,
  UPDATE_MESSAGE_SUBSCRIPTION,
  NEW_REVIEW_SUBSCRIPTION,
} from "./queries";

export const _subscribeToNewMessages = (subscribeToMore) => {
  subscribeToMore({
    document: NEW_MESSAGE_SUBSCRIPTION,
    updateQuery: (prev, { subscriptionData }) => {
      if (!subscriptionData) return prev;
      const { newMessage } = subscriptionData.data;
      const exists = prev.messages.messageList.find(
        ({ id }) => id === newMessage.id
      );

      if (exists)
        return {
          ...prev,
          messages: {
            messageList: [...prev.messages.messageList],
            count: prev.messages.messageList.length,
            __typename: prev.messages.__typename,
          },
        };

      return {
        ...prev,
        messages: {
          messageList: [newMessage, ...prev.messages.messageList],
          count: prev.messages.messageList.length + 1,
          __typename: prev.messages.__typename,
        },
      };
    },
  });
};

export const _subscribeToUpdates = (subscribeToMore) => {
  subscribeToMore({
    document: UPDATE_MESSAGE_SUBSCRIPTION,
    updateQuery: (data) => {
      return { ...data };
    },
  });
};

export const _subscribeToNewReview = (subscribeToMore) => {
  subscribeToMore({
    document: NEW_REVIEW_SUBSCRIPTION,
    updateQuery: (prev, { subscriptionData }) => {
      if (!subscriptionData) return prev;
      const messageList = prev.messages.messageList;
      const { newReview } = subscriptionData.data;

      messageList.forEach((message) => {
        if (message.id === newReview.message.id) {
          message.reviews.push({
            id: newReview.id,
            text: newReview.text,
            __typename: message.reviews[0].__typename,
          });
        }
      });

      return {
        ...prev,
        messages: {
          ...prev.messages,
        },
      };
    },
  });
};
