import React from "react";

import MessageForm from "../../components/MessageForm";
import "./index.css";

import MessageList from "../../components/MessageList";

function App() {
  return (
    <div className="App container p-3">
      <MessageForm />
      <MessageList />
    </div>
  );
}

export default App;
