import React, { useState } from "react";
import { Mutation } from "react-apollo";
import { MESSAGE_QUERY, MESSAGE_REVIEW_QUERY } from "../../queries";

const ReviewForm = ({ messageId, toggleForm }) => {
  const [text, setText] = useState("");

  const _updateStoreAfterAddingReview = (store, newReview, messageId) => {
    const orderBy = "createdAt_DESC";
    const data = store.readQuery({
      query: MESSAGE_QUERY,
      variables: {
        orderBy,
      },
    });
    const reviewedMessage = data.messages.messageList.find(
      (message) => message.id === messageId
    );

    reviewedMessage.reviews.push(newReview);
    store.writeQuery({ query: MESSAGE_QUERY, data });
    toggleForm(false);
  };

  return (
    <div className="mt-3">
      <div className="form-group">
        <textarea
          className="form-control"
          onChange={(e) => setText(e.target.value)}
          placeholder="Review Text"
          autoFocus
          value={text}
          cols={3}
        ></textarea>
      </div>
      <Mutation
        mutation={MESSAGE_REVIEW_QUERY}
        variables={{ messageId, text }}
        update={(store, { data: { postReview } }) => {
          _updateStoreAfterAddingReview(store, postReview, messageId);
        }}
      >
        {(postMutation) => (
          <button className="btn btn-primary" onClick={postMutation}>
            Add
          </button>
        )}
      </Mutation>
    </div>
  );
};

export default ReviewForm;
