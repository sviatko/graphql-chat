import React from "react";

const ReviewItem = ({ text }) => <p>{text}</p>;

export default ReviewItem;
