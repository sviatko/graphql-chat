import React from "react";

const Icon = ({ faIcon, text, onClick }) => (
  <div className="wrapper" onClick={onClick}>
    <i className={faIcon}></i>
    <span>{text}</span>
  </div>
);

export default Icon;
