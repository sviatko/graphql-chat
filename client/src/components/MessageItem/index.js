import React from "react";
import { Mutation } from "react-apollo";

import ReviewList from "../ReviewList";
import Icon from "../Icons";
import { GIVE_LIKE_MUTATION, GIVE_DISLIKE_MUTATION } from "../../queries";

const MessageItem = ({ id, text, likes, dislikes, reviews }) => {
  return (
    <div className="card mt-3">
      <div className="card-body">
        <div className="card-title">{text}</div>
        <div className="d-flex justify-content-center">
          <Mutation mutation={GIVE_LIKE_MUTATION} variables={{ messageId: id }}>
            {(postMutation) => (
              <Icon
                faIcon="fa fa-thumbs-o-up"
                text={likes}
                onClick={postMutation}
              />
            )}
          </Mutation>
          <Mutation
            mutation={GIVE_DISLIKE_MUTATION}
            variables={{ messageId: id }}
          >
            {(postMutation) => (
              <Icon
                faIcon="fa fa-thumbs-o-down"
                text={dislikes}
                onClick={postMutation}
              />
            )}
          </Mutation>
        </div>
        <ReviewList messageId={id} reviews={reviews} />
      </div>
    </div>
  );
};

export default MessageItem;
