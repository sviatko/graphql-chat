import React, { useState } from "react";
import { Mutation } from "react-apollo";
import { MESSAGE_QUERY, POST_MESSAGE_MUTATION } from "../../queries";

const MessageForm = () => {
  const [text, setText] = useState("");

  const _updateStoreAfterAddingMessage = (store, newMessage) => {
    const orderBy = "createdAt_DESC";
    const data = store.readQuery({
      query: MESSAGE_QUERY,
      variables: {
        orderBy,
      },
    });

    data.messages.messageList.unshift(newMessage);
    store.writeQuery({
      query: MESSAGE_QUERY,
      data,
    });
  };

  return (
    <div className="card">
      <div className="card-body">
        <div className="card-title">Add new Message</div>

        <div className="form-group">
          <input
            className={"form-control"}
            placeholder="Text"
            value={text}
            onChange={(e) => setText(e.target.value)}
          />
        </div>

        <Mutation
          mutation={POST_MESSAGE_MUTATION}
          variables={{ text }}
          update={(store, { data: { postMessage } }) => {
            _updateStoreAfterAddingMessage(store, postMessage);
          }}
        >
          {(postMutation) => (
            <button className="btn btn-outline-primary" onClick={postMutation}>
              Add
            </button>
          )}
        </Mutation>
      </div>
    </div>
  );
};

export default MessageForm;
