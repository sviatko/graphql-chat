import React, { useState } from "react";

import ReviewItem from "../ReviewItem";
import ReviewForm from "../ReviewForm";

const ReviewList = ({ messageId, reviews }) => {
  const [showReviewForm, setShowReviewForm] = useState(false);
  return (
    <div>
      {reviews.length > 0 && <span className="font-weight-bold">Reviews</span>}
      {reviews.map((review) => (
        <ReviewItem key={`review-${review.id}`} {...review} />
      ))}
      <button
        className="btn btn-outline-secondary"
        onClick={() => setShowReviewForm(!showReviewForm)}
      >
        {showReviewForm ? "Close review" : "Add review"}
      </button>
      {showReviewForm && (
        <ReviewForm messageId={messageId} toggleForm={setShowReviewForm} />
      )}
    </div>
  );
};

export default ReviewList;
