import React, { useState } from "react";
import { Query } from "react-apollo";

import "./index.css";
import MessageItem from "../MessageItem";
import { MESSAGE_QUERY } from "../../queries";
import {
  _subscribeToNewMessages,
  _subscribeToUpdates,
  _subscribeToNewReview,
} from "../../subscribers";

const MessageList = () => {
  const [skip, setSkip] = useState(0);
  const orderBy = "createdAt_DESC";
  const first = 5;

  return (
    <Query query={MESSAGE_QUERY} variables={{ orderBy, skip, first }}>
      {({ loading, error, data, subscribeToMore }) => {
        if (loading)
          return (
            <div className="text-center mt-3 font-weight-bold">Loading...</div>
          );
        if (error)
          return (
            <div className="text-center mt-3 font-weight-bold">
              Fetch error...
            </div>
          );

        _subscribeToNewMessages(subscribeToMore);
        _subscribeToUpdates(subscribeToMore);
        _subscribeToNewReview(subscribeToMore);

        const {
          messages: { messageList, count, currentPage },
        } = data;
        const pages = Array(Math.ceil(count / first))
          .join()
          .split(",")
          .map(
            function (a) {
              return this.i++;
            },
            { i: 1 }
          );
        console.log(currentPage);
        return (
          <div className="message-list">
            {messageList.map((item) => (
              <MessageItem key={`message-${item.id}`} {...item} />
            ))}
            <div className="pagination">
              {pages.map((page) => (
                <span
                  key={page}
                  className={`pagination-item ${
                    currentPage === page || (currentPage === 0 && page === 1)
                      ? "active"
                      : ""
                  }`}
                  onClick={() => setSkip(page)}
                >
                  {page}
                </span>
              ))}
            </div>
          </div>
        );
      }}
    </Query>
  );
};

export default MessageList;
