import gql from "graphql-tag";

export const MESSAGE_QUERY = gql`
  query messageQuery($orderBy: OrderByInput, $skip: Int!, $first: Int!) {
    messages(orderBy: $orderBy, skip: $skip, first: $first) {
      count
      currentPage
      messageList {
        id
        text
        likes
        dislikes
        reviews {
          id
          text
        }
      }
    }
  }
`;

export const NEW_MESSAGE_SUBSCRIPTION = gql`
  subscription {
    newMessage {
      id
      text
      likes
      dislikes
      reviews {
        id
        text
      }
    }
  }
`;

export const NEW_REVIEW_SUBSCRIPTION = gql`
  subscription {
    newReview {
      id
      text
      message {
        id
      }
    }
  }
`;

export const UPDATE_MESSAGE_SUBSCRIPTION = gql`
  subscription {
    update {
      id
      text
      likes
      dislikes
    }
  }
`;

export const POST_MESSAGE_MUTATION = gql`
  mutation PostMutation($text: String!) {
    postMessage(text: $text) {
      id
      text
      likes
      dislikes
      reviews {
        id
        text
      }
    }
  }
`;

export const MESSAGE_REVIEW_QUERY = gql`
  mutation PostMutation($messageId: String!, $text: String!) {
    postReview(messageId: $messageId, text: $text) {
      id
      text
      message {
        likes
        dislikes
        id
      }
    }
  }
`;

export const GIVE_LIKE_MUTATION = gql`
  mutation PostMutation($messageId: String!) {
    postGiveLike(messageId: $messageId) {
      id
      text
      likes
      dislikes
      reviews {
        id
        text
      }
    }
  }
`;

export const GIVE_DISLIKE_MUTATION = gql`
  mutation PostMutation($messageId: String!) {
    postGiveDislike(messageId: $messageId) {
      id
      text
      likes
      dislikes
      reviews {
        id
        text
      }
    }
  }
`;
